window.onload = dimenciona;

function dimenciona() {
    let tela = document.documentElement.clientHeight;
    let tamanhoBody = document.body.scrollHeight;
    // let altura = tela - 323;

    if (tamanhoBody < tela) {
        document.getElementById('footer').className = 'footer footer-fixed';
    } else {
        document.getElementById('footer').className = 'footer';        
    }
}

$(function () {
    $('.datePicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        orientation: 'bottom'
    });
});

window.onscroll = encolhe;

function encolhe() {
    if(document.body.scrollTop > 91 || document.documentElement.scrollTop > 91) {
        document.getElementById('barra').className = 'page-header-bg header-bg-cadastro hidden-sm hidden-xs header-bg-cadastro-encolhe';
        document.getElementById('logo').className = 'col-md-1 col-lg-1 logo-cadastro logo-cadastro-encolhe';
        if(document.documentElement.clientWidth >= 1200) {
            document.getElementById('container-step').className = 'col-md-11 col-lg-11 container-step container-step-encolhe-lg';
        } else {
            document.getElementById('container-step').className = 'col-md-11 col-lg-11 container-step container-step-encolhe';            
        }
        document.getElementById('bola').className = 'col-md-1 step step-end step-some';
        document.getElementById('text-step').className = 'text-step text-done text-step-some';
        document.getElementById('text-step1').className = 'text-step text-step-some';
        document.getElementById('text-step2').className = 'text-step text-step-some';
        document.getElementById('text-step3').className = 'text-step text-step-some';
        document.getElementById('text-step4').className = 'text-step text-step-some';
        document.getElementById('text-step5').className = 'text-step text-step-some';
        document.getElementById('text-step6').className = 'text-step text-step-end text-step-some';
    } else {
        document.getElementById('barra').className = 'page-header-bg header-bg-cadastro hidden-sm hidden-xs';
        document.getElementById('logo').className = 'col-md-1 col-lg-1 logo-cadastro';
        document.getElementById('container-step').className = 'col-md-11 col-lg-11 container-step';
        document.getElementById('bola').className = 'col-md-1 step step-end';
        document.getElementById('text-step').className = 'text-step text-done';
        document.getElementById('text-step1').className = 'text-step';
        document.getElementById('text-step2').className = 'text-step';
        document.getElementById('text-step3').className = 'text-step';
        document.getElementById('text-step4').className = 'text-step';
        document.getElementById('text-step5').className = 'text-step';
        document.getElementById('text-step6').className = 'text-step text-step-end';
    }
}