export default class StringHelper {
    constructor() {
        throw 'Não é possível instanciar StringHelper';
    }

    static limpaFormato(dadoFormatado) {
        if(dadoFormatado) {
            let dadoLimpo = dadoFormatado.replace(/[^\d]+/g, '');
            return dadoLimpo;
        }
        return null;
    }
}