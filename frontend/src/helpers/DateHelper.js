import moment from 'moment';

export default class DateHelper {
    
    constructor() {
        throw 'Não é possível instanciar DateHelper';
    }

    static convertDateToNumber(dataPtbr) {
        let dataApi = moment(dataPtbr, 'DD/MM/YYYY').format('MMM DD, YYYY');
        dataApi = moment(dataApi).valueOf();
        return dataApi;
    }
    
    static convertNumberToDate(numero) {
        return moment(numero).format("DD/MMM/YYYY");
    }

    static convertDateToPtbr(date) {
        return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
    }

}