export default class IdHelper {
    constructor() {
        throw 'Não é possível instanciar IdHelper';
    }

    static obtemId(objeto) {
        let href = objeto._links.self.href;
        let hrefData = href.split('/');
        let last = hrefData.length - 1;
        let id = hrefData[last];
        return id;
    }

    static obtemIdDeHref(objeto) {
        let href = objeto.href;
        let grupos = href.match(/(\/)(\d+)/);
        let id = grupos[2];
        // let hrefData = href.split('/');
        // let last = hrefData.length - 1;
        // let id = hrefData[last];
        return id;
    }
}