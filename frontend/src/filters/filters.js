import Vue from 'vue';
import moment from 'moment';
import IdHelper from '@/helpers/IdHelper';

Vue.filter('simNao', valor => valor === true ? 'Sim' : 'Não')

Vue.filter('cpfMask', cpf => {
    if(cpf && cpf.length < 14) {
        return cpf.slice(0, 3) + '.' + cpf.slice(3, 6) + '.' + cpf.slice(6, 9) + '-' + cpf.slice(9)
    } else {
        return cpf;
    }
})

Vue.filter('monetarioMask', valor => {
    let str = valor.toString();
    if(!str.match(/[,.]/)) {  // Exemplo 3000
        str = str + '00';
    }
    if(str.charAt(str.length-2) == '.') {  // Exemplo 3000.0
        str = str + '0';
    }
    if(str.match(/[,.]/)) {  // Exemplo 3000.00
        str = str.replace(/[\D]+/g,'');
    }
    let formatado = str.replace(/([0-9]{2})$/g, ",$1");
    if(formatado.length > 6) {
        return formatado.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    } else {
        return formatado;
    }
})

Vue.filter('formataData', valor => {
    if (valor) {
      return moment(String(valor)).format('DD/MM/YYYY');
    }
})

Vue.filter('bancoFilter', valor => {
    let bancos = JSON.parse(sessionStorage.getItem('bancos'));
    let banco = bancos.find(banco => banco._links.self.href == valor);
    return (banco) ? banco.nome : 'Não encontrado'; 
})

Vue.filter('contaBancariaFilter', valor => {
    let contas = JSON.parse(sessionStorage.getItem('tiposContasBancarias'));
    let conta = contas.find(conta => conta.name == valor);
    return (conta) ? conta.displayName : 'Não encontrado';
})

Vue.filter('aplicacaoFilter', valor => {
    let aplicacoes = JSON.parse(sessionStorage.getItem('tiposAplicacoesBancarias'));
    let aplicacao = aplicacoes.find(aplicacao => aplicacao.name == valor);
    return (aplicacao) ? aplicacao.displayName : 'Não encontrado';
})

Vue.filter('cartaoFilter', valor => {
    let cartoes = JSON.parse(sessionStorage.getItem('tiposCartoesCreditos'));
    let cartao = cartoes.find(cartao => cartao.name == valor);
    return (cartao) ? cartao.displayName : 'Não encontrado';
})

Vue.filter('veiculoFilter', valor => {
    let veiculos = JSON.parse(sessionStorage.getItem('tiposVeiculos'));
    let veiculo = veiculos.find(veiculo => veiculo.name == valor);
    return (veiculo) ? veiculo.displayName : 'Não encontrado';
})

Vue.filter('imovelFilter', valor => {
    let imoveis = JSON.parse(sessionStorage.getItem('tiposImoveis'));
    let imovel = imoveis.find(imovel => imovel.name == valor);
    return (imovel) ? imovel.displayName : 'Não encontrado';
})

Vue.filter('despesaFilter', valor => {
    let despesas = JSON.parse(sessionStorage.getItem('tiposDespesas'));
    let despesa = despesas.find(despesa => despesa.name == valor);
    return (despesa) ? despesa.displayName : 'Não encontrado';
})
