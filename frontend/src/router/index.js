import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/login'
import NotFound from '@/components/notfound'
import Forbidden from '@/components/forbidden'
import Manutencao from '@/components/manutencao'
import Perfil from '@/components/perfil'
import Sair from '@/components/sair'
import Avisos from '@/components/avisos'
import ProjetoBasico from '@/components/projeto-basico'
import DetalhaProjetoBasico from '@/components/projeto-basico/detalha-projeto'
import FonteBasico from '@/components/projeto-basico/fonte-basico'
import UnidadeBasico from '@/components/projeto-basico/unidade-basico'
import EtapaBasico from '@/components/projeto-basico/etapa-basico'
import RevisaoBasico from '@/components/projeto-basico/revisao-basico'
import DetalhaEtapaBasico from '@/components/projeto-basico/etapa-basico/detalha-etapa'
import DetalhaAtividadeBasico from '@/components/projeto-basico/etapa-basico/detalha-atividade'
import ProjetoExecutivo from '@/components/projeto-executivo'
import DetalhaProjetoExecutivo from '@/components/projeto-executivo/detalha-projeto'
import ContratoExecutivo from '@/components/projeto-executivo/contrato'
import NumerosExecutivo from '@/components/projeto-executivo/numeros'
import ExecucaoExecutivo from '@/components/projeto-executivo/execucao'
import UnidadeExecutivo from '@/components/projeto-executivo/unidade'
import EtapaExecutivo from '@/components/projeto-executivo/etapa'
import RevisaoExecutivo from '@/components/projeto-executivo/revisao'
import DetalhaEtapaExecutivo from '@/components/projeto-executivo/etapa/detalha-etapa'
import DetalhaAtividadeExecutivo from '@/components/projeto-executivo/etapa/detalha-atividade'
import Empresas from '@/components/empresas'

Vue.use(Router)

const router = new Router({
  	mode: 'history',
  	base: '/sgp/',
  	routes: [
    	{
			path: '/',
			name: 'Login',
			component: Login,
			meta: {requiresAuth: false}
		},
		{
			path: '/forbidden',
			name: 'Forbidden',
			component: Forbidden,
			meta: {requiresAuth: false}
		},
		{
			path: '/manutencao',
			name: 'Manutencao',
			component: Manutencao,
			meta: {requiresAuth: false}
		},
		{
			path: '*',
			name: 'NotFound',
			component: NotFound,
			meta: {requiresAuth: false}
		},
		{
			path: '/sair',
			name: 'Sair'
		},
		{
			path: '/perfil',
			name: 'Perfil',
			component: Perfil,
			meta: {stepNumber: 0}
		},
		{
			path: '/avisos',
			name: 'Avisos',
			component: Avisos,
			meta: {stepNumber: 0}
		},
		{
			path: '/projeto_basico',
			name: 'ProjetoBasico',
			component: ProjetoBasico,
			meta: {
				stepNumber: 0,
				projetosActive: true
			}
		},
		{
			path: '/projeto_basico/detalha_projeto',
			name: 'DetalhaProjetoBasico',
			component: DetalhaProjetoBasico,
			meta: {
				stepNumber: 1,
				stepName: 'Projeto',
				projetosActive: true
			}
		},
		{
			path: '/fonte_basico',
			name: 'FonteBasico',
			component: FonteBasico,
			meta: {
				stepNumber: 2,
				stepName: 'Fonte',
				projetosActive: true
			}
		},
		{
			path: '/unidade_basico',
			name: 'UnidadeBasico',
			component: UnidadeBasico,
			meta: {
				stepNumber: 3,
				stepName: 'Unidade',
				projetosActive: true
			}
		},
		{
			path: '/etapa_basico',
			name: 'EtapaBasico',
			component: EtapaBasico,
			meta: {
				stepNumber: 4,
				stepName: 'Etapas',
				projetosActive: true
			}
		},
		{
			path: '/etapa_basico/detalha_etapa',
			name: 'DetalhaEtapaBasico',
			component: DetalhaEtapaBasico,
			meta: {
				stepNumber: 5,
				stepName: 'Etapa',
				projetosActive: true
			}
		},
		{
			path: '/etapa_basico/detalha_atividade',
			name: 'DetalhaAtividadeBasico',
			component: DetalhaAtividadeBasico,
			meta: {
				stepNumber: 6,
				stepName: 'Atividade',
				projetosActive: true
			}
		},
		{
			path: '/revisao_basico',
			name: 'RevisaoBasico',
			component: RevisaoBasico,
			meta: {
				stepNumber: 7,
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo',
			name: 'ProjetoExecutivo',
			component: ProjetoExecutivo,
			meta: {
				stepNumber: 0,
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/detalhe',
			name: 'DetalhaProjetoExecutivo',
			component: DetalhaProjetoExecutivo,
			meta: {
				stepNumber: 8,
				stepName: 'Projeto',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/contrato',
			name: 'ContratoExecutivo',
			component: ContratoExecutivo,
			meta: {
				stepNumber: 9,
				stepName: 'Contrato',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/numeros',
			name: 'NumerosExecutivo',
			component: NumerosExecutivo,
			meta: {
				stepNumber: 10,
				stepName: 'Valores',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/fonte',
			name: 'ExecucaoExecutivo',
			component: ExecucaoExecutivo,
			meta: {
				stepNumber: 11,
				stepName: 'Fonte',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/unidade',
			name: 'UnidadeExecutivo',
			component: UnidadeExecutivo,
			meta: {
				stepNumber: 12,
				stepName: 'Unidade',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/etapa',
			name: 'EtapaExecutivo',
			component: EtapaExecutivo,
			meta: {
				stepNumber: 13,
				stepName: 'Etapas',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/etapa/detalhe',
			name: 'DetalhaEtapaExecutivo',
			component: DetalhaEtapaExecutivo,
			meta: {
				stepNumber: 13,
				stepName: 'Etapa',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/etapa/atividade/detalhe',
			name: 'DetalhaAtividadeExecutivo',
			component: DetalhaAtividadeExecutivo,
			meta: {
				stepNumber: 13,
				stepName: 'Atividade',
				projetosActive: true
			}
		},
		{
			path: '/projeto_executivo/revisao',
			name: 'RevisaoExecutivo',
			component: RevisaoExecutivo,
			meta: {
				stepNumber: 14,
				projetosActive: true
			}
		},
		{
			path: '/cadastro/empresas',
			name: 'Empresas',
			component: Empresas,
			meta: {
				cadastroActive: true
			}
		}
	]
})

router.beforeEach((to, from, next) => {
  	Vue.http.headers.common['Authorization'] = sessionStorage.token;

	if (to.path == '/' && sessionStorage.token) { next({ name: 'Avisos' }); return; }

	if (to.meta.requiresAuth === false) { next(); return; }

	if (!sessionStorage.token) { next({ path: '/' }); return; }

	if (to.name == 'Sair') { Sair.logout(router.app, next); return; }

	next();
});

export default router;