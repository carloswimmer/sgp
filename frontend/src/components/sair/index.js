export default {
  logout(app, next){
    app.$refs.modal.confirm('Deseja realmente sair do sistema?', {
      yes: () => {
        app.isAuthenticated = false;
        sessionStorage.token = '';
        next({ path: '/' }); 
        return; 
      },
      title: 'Saindo do Sistema'
    });
  }
}
