# Dependencies

* @/components/shared/botao

## Exemplo de uso:

### Componente pai

* Incluir em **template**

```html
<side-modal titulo="Dependência" subtitulo="Etapa" subtDetalhe="Entrega de Material"
    @fundoEscuro="escureceFundo()" @fundoClaro="clareiaFundo()" >
    <div class="row row-field">
        <!-- Conteúdo do modal -->
    </div>...
</side-modal>
```

* Incluir em **script**

```javascript
import SideModal from '@/components/shared/side-modal'
```

```javascript
methods: {
    abreSideModal() {
        this.$emit('sideModal')
    },
    escureceFundo () {
        this.$emit ('escureceDiv')
    },
    clareiaFundo () {
        this.$emit ('clareiaDiv')
    }
},
```

```javascript
components: {
    'lista-atividades-componente': AtividadesComponente,
    'side-modal': SideModal
}
```

* Botão que abre o side-modal

```html
<botao estilo="novo-modal" rotulo="Novo" icone="novo-modal"
    toggle="" target="" @clicado="abreSideModal()" >
</botao>
```

---

### App.vue

* Incluir na primeira linha (possível) do **template**

```html
<div class="overlay" ></div>
```

* Incluir na tag **router-view**

```html
<router-view @escureceDiv="escurece()" @clareiaDiv="clareia()"></router-view>
```

* Incluir em **script**

```javascript
methods: {
    escurece () {
        document.querySelector('.overlay').style.backgroundColor = 'rgba(0,0,0,0.2)'
        document.querySelector('.overlay').style.zIndex = '1990'
    },
    clareia () {
        document.querySelector('.overlay').style.backgroundColor = 'rgba(0,0,0,0)'
        document.querySelector('.overlay').style.zIndex = '0'
    }
}
```

* Incluir em **style**

```css
.overlay {
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    right: 0;
    z-index: 0;
    transition: 0.5s;
}
```