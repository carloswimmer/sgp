// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@/main.less';

import Vue from 'vue';
import App from './App';
import router from './router';
import data from './config';
import VueResource from 'vue-resource';
import toastr from 'vue-toastr';
import autoProgress from 'vue-auto-progress';
import modal from '@/components/common/Modal';
import fieldValidation from 'vue-bootstrap-validate';
import ptBR from 'vee-validate/dist/locale/pt_BR';
import datePicker from 'vue-bootstrap-datetimepicker'; 
import VueMask from 'v-mask';
import money from 'v-money';
import "./filters/filters";

window.jQuery = require('jquery');
require('bootstrap');
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';

Vue.use(datePicker);
Vue.use(VueMask);
Vue.use(money, { decimal: ',', thousands: '.', precision: 2 });
Vue.use(VueResource);
Vue.http.options.root = data.apiUrl;
Vue.config.productionTip = false;

fieldValidation.locale(ptBR, 'pt_BR');
Vue.component('field-validation', fieldValidation);

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	data,
	template: '<div><App/><modal ref="modal" /><toastr ref="toastr" /><auto-progress /></div>',
	components: { App, modal, toastr, autoProgress }
})
