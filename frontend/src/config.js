import env from '../env'

export default {
	sistema: {
		nome: 'Sistema Gestor de Projetos',
		sigla: 'SGP',
		descricao: 'Sistema de gestão, cadastramento e acompanhamento de projetos.'
	},

	usuario: {
		user_name: sessionStorage.user_name,
		name: sessionStorage.name,
		authorities: sessionStorage.authorities,
		firstName: sessionStorage.firstName
	},

	isAuthenticated: !!sessionStorage.token,
	apiUrl: env.apiUrl,
	client_id: env.client_id,
	secret: env.secret
}